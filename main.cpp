#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include "Book.h"


class Node {
public:
    Book book;
    Node* next;
};

void printbook(Book *book) {
    std::cout<<std::endl;
    std::cout<<"Title: " << book->getTitle()<<std::endl;
    std::cout<<"ISBN: "<<book->getISBN()<<std::endl;
    std::cout<<"Quantity: "<<book->getQty()<<std::endl;
    std::cout<<"Authors: "<<std::endl;
    int count = 1;
    for(auto v : book->getAuthor()) {
        std::cout<< count << ": " <<v <<std::endl;
        count++;
    }
    std::cout<<std::endl;
}

Node *node_construct(Book value, Node *next)
{
    Node *node = new Node();
    node->book.setTitle(value.getTitle());
    node->book.setQty(value.getQty());
    node->book.setISBN(value.getISBN());
    node->book.setAuthor(value.getAuthor());
    node->next = next;
    return node;
}

Node *push(Node *head, Book value)
{
    return node_construct(value, head);
}

Book *searchBook(Node *head) {
    std::string title;
    std::cout<<"Enter title of the book to search: ";
    std::cin.ignore();
    getline(std::cin, title);
    if(head == NULL) {
        return NULL;
    }
    Node *current;
    for (current = head; current->next != NULL; current = current->next) {
        if(current->book.getTitle().compare(title) == 0) {
            return &(current->book);
        }
    }
    if(current->book.getTitle().compare(title) == 0) {
        return &(current->book);
    }
    return NULL;
}

void print_list(Node *head)
{
    if (head == NULL) {
        printf("empty list");
        return;
    }

    Node *current;

    for (current = head; current->next != NULL; current = current->next) {
        printbook(&(current->book));
    }
    printbook(&(current->book));
}
void delBook(Node *head) {
    std::string title;
    std::cout<<"Enter title of the book to delete: ";
    std::cin.ignore();
    getline(std::cin, title);
    if(head == NULL) {
        std::cout << "No book with this title exists!" << std::endl;
        return;
    }
    // When node to be deleted is head node
    if(head->book.getTitle().compare(title) == 0)
    {
        if(head->next == NULL)
        {
            std::cout << "There is only one node." <<
                 " The list can't be made empty "<<std::endl;
            return;
        }

        head->book.setTitle(head->next->book.getTitle());
        head->book.setISBN(head->next->book.getISBN());
        head->book.setQty(head->next->book.getQty());
        head->book.setAuthor(head->next->book.getAuthor());

        Node *temp = head->next;
        head->next = head->next->next;
        free(temp);
        std::cout<<"Book has been deleted"<<std::endl;

        return;
    }

    Node *prev = head;
    while(prev->next != NULL && prev->next->book.getTitle().compare(title) != 0)
        prev = prev->next;

    if(prev->next == NULL)
    {
        std::cout << "No book with this title exists!" << std::endl;
        return;
    }
    Node *temp = prev->next;
    prev->next = prev->next->next;
    free(temp);
    std::cout<<"Book has been deleted"<<std::endl;
    return;
}



Node *addBook(Node *head) {
    Book book;
    std::string title;
    std::string ISBN;
    int quantity;
    int number_authors;
    std::vector<std::string> authors;
    std::cout<<"Enter title: ";
    std::cin.ignore();
    getline(std::cin, title);
    std::cout<<"Enter ISBN: ";
    getline(std::cin, ISBN);
    std::cout<<"Enter Number of authors: ";
    std::cin>>number_authors;
    std::cin.ignore();
    std::cout<<"Enter Author names: \n";
    for(int i = 0; i < number_authors; i++) {
        std::string temp;
        getline(std::cin, temp);
        authors.push_back(temp);
    }
    std::cout<<"Enter Quantity: ";
    std::cin>>quantity;
    book.setISBN(ISBN);
    book.setQty(quantity);
    book.setTitle(title);
    book.setAuthor(authors);
    head = push(head, book);
    return head;
}
std::string get_list(Node *head) {
    std::string str = "";
    if (head == NULL) {
        return "";
    }

    Node *current;

    for (current = head; current->next != NULL; current = current->next) {
        str += current->book.getTitle();
        str += "\t";
        std::vector<std::string> authors = current->book.getAuthor();
        for(int i = 0; i < authors.size() - 1; i++) {
            str += authors.at(i);
            str += ";";
        }
        str += authors.at(authors.size() - 1);
        str += "\t";
        str += current->book.getISBN();
        str += "\t";
        str += std::to_string(current->book.getQty());
        str += "\n";
    }
    str += current->book.getTitle();
    str += "\t";
    std::vector<std::string> authors = current->book.getAuthor();
    for(int i = 0; i < authors.size() - 1; i++) {
        str += authors.at(i);
        str += ";";
    }
    str += authors.at(authors.size() - 1);
    str += "\t";
    str += current->book.getISBN();
    str += "\t";
    str += std::to_string(current->book.getQty());
    str += "\n";
    return str;
}
void saveData(std::string filename, Node *head) {
    std::ofstream file;
    file.open(filename, std::ios::out);
    if (file.is_open())
    {
        file << get_list(head);
        file.close();

    }
    else std::cout << "Unable to open file";
}

int main(int argv, char *argc[])
{

    Node* head = NULL;
    std::ifstream file;
    if(argv == 2) {
        file.open(argc[1], std::ios::in); //open a file to perform read operation using file object
        if (file.is_open()){   //checking whether the file is open
            std::string tp;
            while(getline(file, tp)){ //read data from file object and put it into string.
                int start = 0;
                std::string del = "\t";
                int end = tp.find(del);
                Book book;
                book.setTitle(tp.substr(start, end - start));
                start = end + del.size();
                end = tp.find(del, start);
                std::string authors = tp.substr(start, end - start);
                start = end + del.size();
                end = tp.find(del, start);
                book.setISBN(tp.substr(start, end - start));
                start = end + del.size();
                end = tp.find(del, start);
                std::stringstream degree(tp.substr(start, end - start));
                int x = 0;
                degree >> x;
                book.setQty(x);

                start = 0;
                del = ";";
                std::vector<std::string> author;
                end = authors.find(del);
                while (end != -1) {
                    author.push_back(authors.substr(start, end - start));
                    start = end + del.size();
                    end = authors.find(del, start);
                }
                author.push_back(authors.substr(start, end - start));
                book.setAuthor(author);
                head = push(head, book);
            }
            file.close(); //close the file object.
        }
    }

    //Menu display
    while(true) {
        std::cout<<"1. Add a book"<<std::endl;
        std::cout<<"2. Search a book"<<std::endl;
        std::cout<<"3. Delete a book"<<std::endl;
        std::cout<<"4. Print all books"<<std::endl;
        std::cout<<"5. exit"<<std::endl;
        int choice;
        std::cin>>choice;
        switch(choice) {
            case 1:
                head = addBook(head);
                std::cout<<"Book has been added"<<std::endl;
                break;
            case 2: {
                Book *book = searchBook(head);
                if (book == NULL) {
                    std::cout << "No book with this title exists!" << std::endl;
                } else {
                    printbook(book);
                }
            }
                break;
            case 3:
                delBook(head);
                break;
            case 4:
                print_list(head);
                break;
            case 5:
                saveData(argc[1], head);
                return 0;
            default:
                std::cout<<"Wrong choice!";
        }
    }
}