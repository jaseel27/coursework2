#include <iostream>
#include <vector>
#include "Book.h"

std::string Book::getTitle() {
        return title;
    }
    std::vector<std::string> Book::getAuthor() {
        return author;
    }
    std::string Book::getISBN() {
        return ISBN;
    }
    int Book::getQty() {
        return qty;
    }

    void Book::setTitle(std::string tit) {
        title = tit;
    }

    void Book::setAuthor(std::vector<std::string> aut) {
        author = aut;
    }
    void Book::setISBN (std::string ISB) {
        ISBN = ISB;
    }
    void Book::setQty (int qt) {
        qty = qt;
    }