#ifndef _BOOK_H_
#define _BOOK_H_

#include <iostream>

class Book {
private:
     std::string title;
     std::vector< std::string> author;
     std::string ISBN;
    int qty;
public:
    std::string getTitle();
     std::vector< std::string> getAuthor();
     std::string getISBN();
    int getQty();

    void setTitle( std::string tit);
    void setAuthor( std::vector< std::string> aut);
    void setISBN ( std::string ISB);
    void setQty (int qt);
};
#endif